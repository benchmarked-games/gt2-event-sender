/*
 * Example of use:
 *
 * (function() {
        // Instantiate the GT2EventSender class
 *      sender = GT2EventSender(null, 'example game', '1.0', canvas_context);
 *
 *      // Send the game_load message
 *      sender.send(GT2EventSender.LOAD, payload);
 *
 *      // Send the game_start message
 *      sender.send(GT2EventSender.START, payload);
 *
 *      // Send the first game event
 *      sender.send(GT2EventSender.EVENT, payload);
 *
 *      // Reserve a msg_num here.  This can be useful e.g. for keystroke
 *      // events: the message number is reserved when the keypress
 *      // happends…
 *      var second_event_num = sender.reserve_msg_num();
 *
 *      // …then we send another event that actually happened after the
 *      // keypress, but before releasing the key…
 *      sender.send(EventSender.EVENT, payload);
 *
 *      // …and finally we send the keypress event when the key is released,
 *      // with the correct message number.  This way we can include the
 *      // calculated keypress length, too.
 *      var second_event_timestamp = sender.get_msg_num_timestamp(second_event_num);
 *      // Do the calculation
 *      sender.send(EventSender.EVENT, payload, second_event_num);
 *
 *      // At the end, send the game_end event
 *      sender.send(EventSender.END, payload);
 * })
 */

/**
 * This callback type is called `requestCallback` and is displayed as a global symbol.
 *
 * @callback requestCallback
 * @param {boolean} success - if the request was successful
 * @param {Object} response - response object is case of successful response
 * @param {string} error - error message is case of not successful response
 */

 /**
  * This callback contains data about the status of resending the previously failed events
  * 
  * @callback resendStatusCallback
  * @param {Number} eventIndex - the index of the send event
  * @param {boolean} success - if the last sent event was successful
  * @param {Object} response - response object is case of successful response from last event
  * @param {string} error - error message is case of not successful response from last event
  * @param {Number} eventCount - the count of all the re-sending events
  */

 const DEFAULT_CONFIG = {
    event_url: 'https://benchmarked.games/api/v1/game-data/',
    retry_count: 2,
    event_send_timeout: 30000,
    event_retry_timeout: 10000,
    send_events: true,
    log_events: false
};

/**
 * GT2 Game Event sending class
 * @constructor
 *
 * Conforms with GT2 Game Message Standard version 1.
 *
 * @param {object} config - Configuration data
 * @param {String} game_name - The name of the game
 * @param {String} game_version - The version of the game
 * @param {Object} game_context - The canvas context of the game, if any
 */
class EventSender {
    constructor(config, game_name, game_version, game_context, levelpack, gameplay_id) {
        this.game_name = game_name;
        this.game_version = game_version;
        this.game_context = game_context;
        this.game_id = null;
        this.gameplay_id = gameplay_id;
        this.gameplay_session_id = null;
        this.player_id = null;
        this.levelpack = levelpack;
        this.level = null;

        this.game_finished = false
        this.failed_events = {}
        this.game_end_msg_num = null;
        this.game_end_payload = null;

        var queryParams = {};
        
        if (this.gameplay_id && this.gameplay_id.startsWith('GP-')) {
            this.gameplay_id = this.gameplay_id.substring(3); // Remove 'GP-' prefix
        } 

        window.location.search.substr(1).split('&').forEach(function (pair) {
            if (pair === '') return;

            var parts = pair.split('=');
            queryParams[parts[0]] = parts[1] &&
                decodeURIComponent(
                    parts[1].replace(/\+/g, ' '));
        });

        if ((config === undefined) || (config === null) || (config === {})) {
            config = DEFAULT_CONFIG;
        }

        this.game_id = queryParams.game_id;
        this.player_id = queryParams.player_id;

        if (config.hasOwnProperty('send_events')) {
            this.send_events = !!config.send_events;
        } else {
            this.send_events = true;
        }

        if (config.hasOwnProperty('log_events')) {
            this.log_events = !!config.log_events;
        } else {
            this.log_events = false;
        }

        if (config.hasOwnProperty('event_url')) {   
            this.event_url = `${config.event_url}event-v2/${this.gameplay_id}`;
        }

        if (config.hasOwnProperty('retry_count')) {
            this.retry_count = Number(config.retry_count);
        } else {
            this.retry_count = DEFAULT_CONFIG.retry_count;
        }

        if (config.hasOwnProperty('event_send_timeout')) {
            this.timeout = Number(config.event_send_timeout);
        } else {
            this.timeout = DEFAULT_CONFIG.event_send_timeout;
        }

        if (config.hasOwnProperty('event_retry_timeout')) {
            this.retry_timeout = Number(config.event_retry_timeout);
        } else {
            this.retry_timeout = DEFAULT_CONFIG.event_retry_timeout;
        }

        // Error checking
        if (this.send_events && !this.event_url) {
            throw new Error('Event URL is not configured');
        }

        // Initial values
        this.msg_num = 0;
        this.load_timestamp = null;
        this.load_sent = false;
        this.start_sent = false;
        this.end_sent = false;
        this.msg_num_timestamps = {};
        this.gameplay_session_id = this.generateSessionId();

        this.xhr = new XMLHttpRequest();
        
        this.boundSend = this.send.bind(this);

        if (typeof window !== 'undefined') {
            window.addEventListener('blur', () => {
                if (!this.game_finished) {
                    this.boundSend('blur', {});
                }
            });
            window.addEventListener('focus', () => {
                if (!this.game_finished) {
                    this.boundSend('focus', {});
                }
            });
        }
    }

    generateSessionId() {
        const sessionIdChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var ret = 'GS-';

        for (var i = 0; i < 12; i++) {
            var num = Math.floor(Math.random() * (sessionIdChars.length));
            ret += sessionIdChars[num];
        }

        return ret;
    }

    /**
     * Reserve a message number for later use.  This is useful for events that
     * should be sent later (like keypresses tat incorporate keypress length),
     * but on the event timeline, happen earlier than some others.
     */
    reserve_msg_num() {
        var msg_num = ++this.msg_num;

        this.msg_num_timestamps[msg_num] = Date.now();

        return msg_num;
    };

    /**
     * Get the timestamp when `msg_num` was reserved.
     */
    get_msg_num_timestamp(msg_num) {
        return this.msg_num_timestamps[msg_num];
    }

    /**
     * Save and store game_end event's msg_num and payload for later reuse.
     * It's optional to call, but useful when failed_events will be sent to
     * parent for later event reconsuction.
     */
    save_game_end_data(msg_num, payload) {
        this.game_end_msg_num = msg_num;
        this.game_end_payload = payload;
    }
    
    /**
     * Save failed event object for later resending usage
     */
    save_failed_event(eventData, tryCount, failReason) {
        // Save failed events, only if resending haven't yet started
        if (!this.game_finished || (this.game_finished && tryCount === 1)) {

            this.failed_events[eventData.msg_n] = {
                event_data: eventData,
                msg_num: eventData.msg_n,
                try_count: tryCount,
                fail_reason: failReason
            };
        }
    }
    
    /**
     * Delete failed event object if a retry logic could send the event
     */
    remove_failed_event(msg_num) {
        // Delete failed events, only if resending haven't yet started
        if (msg_num in this.failed_events && this.game_finished === false) {
            delete this.failed_events[msg_num]
        }
    }

    /**
     * Get the count of failed events
     */
    get_failed_event_count() {
        return Object.keys(this.failed_events).length;
    }

    /**
     * Resend the previously saved failed events
     * @param {Number} event_index - the index of the failed event (based on get_failed_event_count value)
     * @param {resendStatusCallback} status_callback - callback after the event sending
     */
    resend_failed_event(event_index, status_callback) {
        if (event_index === 0 && this.game_finished === false && this.get_failed_event_count() > 0) {
            this.game_finished = true
        }
        const eventCount = Object.keys(this.failed_events).length;

        if (event_index > (eventCount-1) || event_index === null || event_index < 0) {
            throw new Error(`Invalid event_index parameter - ${event_index}`);
        } else if (eventCount <= 0) {
            return
        } else {
            // Send the event
            const event = Object.values(this.failed_events)[event_index];
            const event_data = event.event_data;
            let self = this
            sendFailedEventRequest(event_data, this.event_url, function(success, response, error) {
                if (!self.end_sent) {
                    if (error) {
                        let gameEndTimestamp = self.game_end_msg_num ? (self.msg_num_timestamps[self.game_end_msg_num] || Date.now()) : null;
                        let gameEndRelativeTime = gameEndTimestamp ? (gameEndTimestamp - self.load_timestamp) : null;
                        send_failed_events_to_parent(self.event_url, self.failed_events, self.game_id, self.gameplay_id, self.player_id, self.gameplay_session_id, self.game_end_msg_num, self.game_end_payload, gameEndTimestamp, gameEndRelativeTime, self.game_name, self.levelpack);
                    }
                    status_callback(event_index, success, response, error, eventCount);
                } else {
                    console.log(`[event-sender] try to send status_callback after game-end success=${success} error=${error}`)
                }
            })
        }
    }

    /**
     * Send an event with the given payload and optional message number.
     *
     * @param {String} type - the event type.  It is recommended to use the
     *                        EventSender.LOAD, EventSender.START,
     *                        EventSender.EVENT, and EventSender.END constants.
     * @param {Object} payload - the event payload.  May be null.  If not null,
     *                           it must be an object.
     * @param {Number} msg_num - an optional message number.  If not set, one
     *                           will be generated automatically.
     * @param {requestCallback} callback - callback after the event sending
     */
    send(type, payload, msg_num, callback) {
        try {
            if ((type !== 'load') &&
                (type !== 'start') &&
                (type !== 'event') &&
                (type !== 'end') &&
                this.log_events) {
                throw new Error('Invalid event type!');
            }
    
            // If the 'game_end' event has already been sent, refuse to send
            // anything
            if (this.end_sent) {
                throw new Error(`Trying to send an event after end! | type=${type} | msg_num=${msg_num}`);
            }
    
            // Don't send events after game_finished except game_end event
            if (type !== 'end' && this.game_finished) {
                throw new Error('Do not send events after game finished, expect the game_end');
            }
    
            // If 'game_load' is already sent, don’t send 'game_load' again.
            if ((this.load_sent) && type === 'load') {
                throw new Error('Trying to send load for a second time!');
            }
    
            if (type === 'load') {
                this.load_sent = true;
    
                if ((payload === undefined) || (payload === null)) {
                    payload = {};
                }
    
                var screen = window ? (window.screen || {}) : {};
                var navigator = window ? (window.navigator || {}) : {};
    
                if (this.game_context) {
                    var backingStoreRatio =
                        this.game_context.webkitBackingStorePixelRatio ||
                        this.game_context.mozBackingStorePixelRatio ||
                        this.game_context.msBackingStorePixelRatio ||
                        this.game_context.oBackingStorePixelRatio ||
                        this.game_context.backingStorePixelRatio;
                    payload['backing-store-ratio'] = backingStoreRatio;
                }
    
                payload['game-name'] = this.game_name;
                payload['game-version'] = this.game_version;
                payload['user-agent'] = navigator ? navigator.userAgent : null;
                payload['platform'] = navigator ? navigator.platform : null;
                payload['language'] = navigator ? navigator.language : null;
                payload['languages'] = navigator.languages ? JSON.stringify(navigator.languages) : null;
                payload['screen-avail-height'] = screen.availHeight;
                payload['screen-avail-left'] = screen.availLeft;
                payload['screen-avail-top'] = screen.availTop;
                payload['screen-avail-width'] = screen.availWidth;
                payload['screen-color-depth'] = screen.colorDepth;
                payload['screen-pixel-depth'] = screen.pixelDepth;
                payload['screen-height'] = screen.height;
                payload['screen-width'] = screen.width;
                payload['device-pixel-ratio'] = window ? window.devicePixelRatio : null;
                payload['retry_limit'] = this.retry_count;
            }
    
            if ((this.start_sent) && type === 'start') {
                throw new Error('Trying to send start for a second time!');
            }
    
            if (type === 'start') {
                this.start_sent = true;
            }
    
            // If the message type is 'game_load' or there were no game_load events,
            // set the load timestamp to now
            // TODO: Don't let the game send game_event messages before game_load
            // and game_start are sent, or if game_end is already sent.
            if ((type === 'load') || (this.load_timestamp === null)) {
                this.load_timestamp = Date.now();
            }
    
            // If the message type is 'game_load', the event timestamp is the same
            // as the load timestamp
            var timestamp = (type === 'load') ? this.load_timestamp : Date.now();
    
            // If msg_num is not set, generate a new one.  Otherwise, consider it as
            // a reserved msg_num and take its timestamp from
            // this.msg_num_timestamps (managed by the reserve_msg_num() method), or
            // generate a new one if not present.
            if (msg_num === undefined) {
                msg_num = ++this.msg_num;
            } else {
                timestamp = this.msg_num_timestamps[msg_num] || Date.now();
            }
    
            const tryCount = 1; // Initializing tryCount
    
            const copiedPayload = Object.assign({}, payload);
            // New structure implementation
            const event_data = {
                v: 2,
                g: this.game_name,
                lvl_p: this.levelpack, // Use levelpack property
                gp_s_id: this.gameplay_session_id,
                msg_n: msg_num,
                ts: timestamp,
                msg_t: type,
                tc: tryCount, // Set tc to tryCount
                g_p: copiedPayload // Game-specific data
            };

            if (this.level) {
                event_data.lvl = this.level; // Add lvl property if level exists
            }            
    
            if (this.send_events) {
                sendEventRequest(event_data, this, this.event_url, this.timeout, tryCount, this.retry_count, this.retry_timeout, null, callback);
            }
    
            if (this.log_events) {
                console.log(event_data);
            }
        } catch (error) {
            throw new Error(error);
        }
    }
}

/**
 * Private function to retry an event request if limit is not reached.
 *
 * @param {Object} event_data - The object of the event request payload
 * @param {EventSender} eventSender - instance of the EventSender
 * @param {String} eventUrl - URL of the event service API
 * @param {Number} timeout - Request timeout in milliseconds
 * @param {Number} tryCount - The number of retries
 * @param {Number} retryLimit - The request retry limit
 * @param {Number} retryTimeout - Request retry timeout in milliseconds
 * @param {String} retryReason - The reason of the retry
 * @param {requestCallback} callback - callback after the event sending
 */
function requestRetry(event_data, eventSender, eventUrl, timeout, tryCount, retryLimit, retryTimeout, retryReason, callback) {
    if (tryCount <= retryLimit) {
        // Retry a new event request
        if (eventSender.log_events) {
            console.log(`request retry | tryCount=${tryCount} | retryLimit=${retryLimit}`)
        }

        setTimeout(function(){
            sendEventRequest(event_data, eventSender, eventUrl, timeout, (tryCount+1), retryLimit, retryTimeout, retryReason, callback);
        }, Math.min(2**tryCount,64) * retryTimeout);

    } else {
        // Retry limit reached
        console.error(`Could not send event - Retry limit (${retryLimit}) reached`);
        if (callback) {
            callback(false, null, `Retry limit (${retryLimit}) reached`);
        }
    }
}

/**
 * Private function to send an event request
 *
 * @param {Object} event_data - The object of the event request payload
 * @param {EventSender} eventSender - instance of the EventSender
 * @param {String} eventUrl - URL of the event service API
 * @param {Number} timeout - Request timeout in milliseconds
 * @param {Number} tryCount - The number of retries
 * @param {Number} retryLimit - The request retry limit
 * @param {Number} retryTimeout - Request retry timeout in milliseconds
 * @param {String} retryReason - The reason of the retry
 * @param {requestCallback} callback - callback after the event sending
 */
function sendEventRequest(event_data, eventSender, eventUrl, timeout, tryCount, retryLimit, retryTimeout, retryReason, callback) {
    if (event_data.tc && tryCount) {
        event_data.tc = tryCount;
    }
    if (event_data.g_p && retryReason) {
        event_data.g_p.retry_reason = retryReason;
    }
    var json_data = JSON.stringify(event_data);
    var request = new XMLHttpRequest();
    request.open('POST', eventUrl, true);
    request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
    request.timeout = timeout;
    request.data = json_data;

    request.addEventListener('error', function() {
        eventSender.save_failed_event(event_data, tryCount, 'Request error');
        requestRetry(event_data, eventSender, eventUrl, timeout, tryCount, retryLimit, retryTimeout, 'Request error', callback);
    });

    request.addEventListener('timeout', function() {
        eventSender.save_failed_event(event_data, tryCount, 'Request timeout');
        requestRetry(event_data, eventSender, eventUrl, timeout, tryCount, retryLimit, retryTimeout, 'Request timeout', callback);
    });

    request.addEventListener('readystatechange', function() {
        if (request.readyState == XMLHttpRequest.OPENED) {
            eventSender.pendingEvents++;
        } else if (request.readyState == XMLHttpRequest.DONE) {
            eventSender.pendingEvents--;

            const responseStatus = request.status;
            const responseStatusClass = responseStatus ? responseStatus.toString()[0] : null;
            const responseStatusText = request.statusText;
            const responseText = request.responseText;

            if (request.status === 0) {
                // Network and CORS error - Retry
                if (eventSender.log_events) {
                    console.log(`readystatechange network error | tryCount=${tryCount} | retryLimit=${retryLimit}`)
                }
                eventSender.save_failed_event(event_data, tryCount, `Network error`);
                requestRetry(event_data, eventSender, eventUrl, timeout, tryCount, retryLimit, retryTimeout, `Network error`, callback);
                return;
            } else if (responseStatusClass === "4") {
                // 4xx error handling - Don't retry the request
                if (eventSender.log_events) {
                    console.log(`readystatechange 4xx | tryCount=${tryCount} | retryLimit=${retryLimit}`)
                }
                console.error(`${responseStatus} status response from the API server`);
                if (callback) {
                    callback(false, null, responseStatusText);
                }
                return;
            } else if (responseStatusClass === "5") {
                // 5xx error handling - Retry
                if (eventSender.log_events) {
                    console.log(`readystatechange 5xx | tryCount=${tryCount} | retryLimit=${retryLimit}`)
                }
                eventSender.save_failed_event(event_data, tryCount, `Response error ${responseStatus}`);
                requestRetry(event_data, eventSender, eventUrl, timeout, tryCount, retryLimit, retryTimeout, `Response error ${responseStatus}`, callback);
                return;
            } else {
                // 200, non-4xx and non-5xx response handling
                if (eventSender.log_events) {
                    console.log(`readystatechange non-4xx non-5xx | tryCount=${tryCount} | retryLimit=${retryLimit}`)
                }
                var result;
                try {
                    result = JSON.parse(responseText);
                } catch (error) {
                    console.error('Unexpected response from the API server');
                    if (callback) {
                        callback(false, null, error);
                    }
                    return;
                }

                if (result && result.reason) {
                    console.error(`Event refused - reason=${result.reason}`);
                    if (callback) {
                        callback(false, null, result.reason);
                    }
                } else {
                    eventSender.remove_failed_event(event_data.msg_n);
                    if (event_data.msg_t === 'end') {
                        eventSender.end_sent = true;
                        send_delete_failed_events_to_parent();
                    }
                    if (callback) {
                        callback(true, result, null);
                    }
                }
            }
        }
    });

    request.send(json_data);
}

/**
 * Private function to send a previously failed event (without a retry logic)
 * 
 * @param {Object} event_data - The object of the event request payload
 * @param {String} eventUrl - URL of the event service API
 * @param {requestCallback} callback - callback after the event sending
 */
function sendFailedEventRequest(event_data, eventUrl, callback) {
    var json_data = JSON.stringify(event_data);
    var request = new XMLHttpRequest();
    request.open('POST', eventUrl, true);
    request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
    request.data = json_data;

    let callback_sent = false

    request.addEventListener('error', function() {
        if (!callback_sent) {
            callback(false, null, `Request error`);
            callback_sent = true
        }
    });

    request.addEventListener('timeout', function() {
        if (!callback_sent) {
            callback(false, null, `Request timeout`);
            callback_sent = true
        }
    });

    request.addEventListener('readystatechange', function() {
        if (request.readyState == XMLHttpRequest.DONE) {

            const responseStatus = request.status;
            const responseStatusClass = responseStatus ? responseStatus.toString()[0] : null;
            const responseText = request.responseText;

            if (request.status === 0) {
                // Network and CORS error
                if (!callback_sent) {
                    callback(false, null, 'Network error');
                    callback_sent = true
                }
                return;
            } else if (responseStatusClass === "4" || responseStatusClass === "5") {
                // 4xx or 5xx error
                if (!callback_sent) {
                    callback(false, null, responseStatus);
                    callback_sent = true
                }
                return;
            } else {
                // 200, non-4xx and non-5xx response handling
                var result;
                try {
                    result = JSON.parse(responseText);
                } catch (error) {
                    if (!callback_sent) {
                        callback(false, null, error);
                        callback_sent = true
                    }
                }

                if (result && result.success === false) {
                    if (!callback_sent) {
                        callback(false, null, result.code);
                        callback_sent = true
                    }
                } else {
                    if (!callback_sent) {
                        callback(true, result, null);
                        callback_sent = true
                    }
                }
            }
        }
    });

    request.send(json_data);
}

/**
 * Private function to send the failed evens to the parent (e.q. play-site)
 */
function send_failed_events_to_parent(eventUrl, failedEvents, gameId, gameplayId, playerId, gameplaySessionId, gameEndMsgNum, gameEndPayload, gameEndTimestamp, gameEndRelativeTime, game_name, levelpack) {
    
    let extendedFailedEvents = Object.assign({}, failedEvents);

    // Construct game_end event and add it to failedEvents
    if (gameEndMsgNum && gameEndPayload && gameEndTimestamp && gameEndRelativeTime) {
        let constructedGameEnd = {
            event_data: {
                v: 2,
                g: game_name,
                lvl_p: levelpack,
                gp_s_id: gameplaySessionId,
                msg_n: gameEndMsgNum,
                ts: gameEndTimestamp,
                msg_t: 'end',
                tc: 0,
                g_p: gameEndPayload
            },
            msg_n: gameEndMsgNum,
            fail_reason: ''
        }
        extendedFailedEvents[gameEndMsgNum] = constructedGameEnd;
    }
    
    // Send event to parent
    const message = {
        messageType: 'failed-events',
        eventUrl: eventUrl,
        failedEvents: extendedFailedEvents,
        gameId: gameId,
        gameplayId: gameplayId,
        playerId: playerId,
        timestamp: new Date(),
    };
    parent.postMessage(JSON.parse(JSON.stringify(message)), "*");
}

/**
 * Private function to delete the failed evens to the parent (e.q. play-site)
 */
function send_delete_failed_events_to_parent() {
    parent.postMessage({
        messageType: 'delete-failed-events'
    }, "*");
}


EventSender.LOAD = 'load';
EventSender.START = 'start';
EventSender.EVENT = 'event';
EventSender.END = 'end';
EventSender.BLUR = 'blur';
EventSender.FOCUS = 'focus';
EventSender.LEVEL_START = 'level_start';
EventSender.LEVEL_END = 'level_end';
EventSender.SECTION_START = 'section_start';
EventSender.SECTION_END = 'section_end';


export default EventSender;
export {EventSender};
